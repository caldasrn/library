import java.util.Date;
import java.text.SimpleDateFormat;

public class TestString {
    //Chapter 10: Working with Strings
    public static void main(String[] args) {
        String example = "example.doc";
        example = example.replace(".doc", ".bak");

        System.out.println(example);

        String test1 = "Adjacenc";
        String test2 = "adjacent";

        if (test1.compareTo(test2) == 0) System.out.println("Strings are Equal");
        else if (test1.compareTo(test2) > 0) System.out.println(test1 + " is further forward.");
        else System.out.println(test2 + " is further forward.");

        //Part 2
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date date = new Date();
        System.out.println(date);
        System.out.println(dateFormat.format(date));
        System.out.println(java.time.LocalDate.now());

    }
    
}