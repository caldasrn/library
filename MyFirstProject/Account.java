public abstract class Account implements Detailable{
    private String name;
    private double balance;

    private static double interestRate;
        
    public Account () throws DodgyNameException {
        this.name = "Rod";
        this.balance = 50;
    }

    public Account(String name, double balance) throws DodgyNameException {
        if ("Fingers".equals(name))
            throw new DodgyNameException();
        this.name = name;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws DodgyNameException{
        if ("Fingers".equals(name))
            throw new DodgyNameException();
        this.name = name;
    }
    //Chapter 11: Part 3
    public abstract void addInterest();

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public boolean withdraw(double amount) {
        if (this.balance >= amount){
            this.balance -= amount;
            System.out.println("Withdrawing $" + String.format("%10.2f", amount) + "from your account.");
            return true;
        }
        System.out.println("Your account has insuficient funds.");
        return false;
    }

    public boolean withdraw() {
        return withdraw(20);
    }
    //Chapter 12: Interface
    public String getDetails() {
        return "The Account Holder is " + name + "\nThe Account Balance is: $" + String.format("%10.2f", balance);
    }
}