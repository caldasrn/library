public class HomeInsurance implements Detailable{
    //Chapter 12: Interfaces
    private double premium, excess, amount;

    public HomeInsurance(double premium, double excess, double amount) {
        this.premium = premium;
        this.excess = excess;
        this.amount = amount;
    }

    @Override
    public String getDetails() {
        // TODO Auto-generated method stub
        return "The premium is $" + premium + " The insured amount is " + amount + " The excess is " + excess;
    }

    
}