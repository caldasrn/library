public class SavingsAccount extends Account{
    //Chapter 11: Inheritance and Abstraction

    public SavingsAccount(String name, double balance) {
        super(name, balance);
    }
    
    @Override
    public void addInterest() {
        this.setBalance(this.getBalance() * 1.4);
    }

    public SavingsAccount() {
    }
}