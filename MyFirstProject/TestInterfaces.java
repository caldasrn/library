public class TestInterfaces {
    //Chapter 12: Interfaces
    public static void main(String[] args) {
        Detailable[] arrayDetailables = new Detailable[3];
        arrayDetailables[0] = new CurrentAccount("Rod", 5000.00);
        arrayDetailables[1] = new SavingsAccount("Marcelo", 30.00);
        arrayDetailables[2] = new HomeInsurance(1000, 250, 150000);
        
        for (Detailable detail : arrayDetailables) {
            System.out.println(detail.getDetails());
        }
    }
}